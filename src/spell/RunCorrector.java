package spell;

import java.io.*;
import java.util.*;

import spell.SpellCorrector.NoSimilarWordFoundException;

public class RunCorrector {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SpellCorrectorImpl corrector = new SpellCorrectorImpl();
		String result = "";
		try {
			corrector.useDictionary(args[0]);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("Dictionary File Not Found.");
			return;
		}
		
		try {
			result = corrector.suggestSimilarWord(args[1]);
		} catch (NoSimilarWordFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("Sorry, no similar word was found.");
			return;
		}
		
		System.out.println("Suggestion is: "+result);
	}

}
