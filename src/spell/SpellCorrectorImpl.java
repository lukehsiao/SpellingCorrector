package spell;

import java.io.*;
import java.util.*;

import spell.SpellCorrector.NoSimilarWordFoundException;

public class SpellCorrectorImpl implements SpellCorrector {
	TrieImpl dict;
	
	public SpellCorrectorImpl() {
		dict = new TrieImpl();
	}

	
	/**
	 * Tells this <code>SpellCorrector</code> to use the given file as its dictionary
	 * for generating suggestions. 
	 * @param dictionaryFileName File containing the words to be used
	 * @throws IOException If the file cannot be read
	 */
	@Override
	public void useDictionary(String dictionaryFileName) throws IOException {
		dict = new TrieImpl(); //reset dictionary
		Scanner scan = null;
		File inFile = new File(dictionaryFileName);
		try {
			scan = new Scanner(inFile);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("File not found.");
		}

		while (scan.hasNext()) {
			String currentWord = scan.next();
			dict.add(currentWord);			
		}
		
		//System.out.println(dict.toString());
	}

	/**
	 * Suggest a word from the dictionary that most closely matches
	 * <code>inputWord</code>
	 * @param inputWord
	 * @return The suggestion
	 * @throws NoSimilarWordFoundException If no similar word is in the dictionary
	 */
	@Override
	public String suggestSimilarWord(String inputWord)
			throws NoSimilarWordFoundException {
		// TODO Auto-generated method stub
		
		if (dict.find(inputWord) != null) {
			return inputWord.toLowerCase();
		}
		
		TreeSet<String> edit1 = dict.editDistance1(inputWord);
		TreeSet<String> edit2 = new TreeSet<String>(); //if necessary
		
		//search edit1
		Iterator<String> it = edit1.iterator();
		int maxValue = 0;
		String suggestion = "";
		while (it.hasNext()) {
			String currentWord = it.next();
			if (dict.find(currentWord) != null && dict.find(currentWord).getValue() > maxValue) {
				maxValue = dict.find(currentWord).getValue();
				suggestion = currentWord;
			}
		}
		
		if (suggestion.equals("")) {
			it = edit1.iterator();
			while (it.hasNext()) {
				String d1 = it.next();
				edit2.addAll(dict.editDistance1(d1));
			}
		}
		
		
		//search edit2
		Iterator<String> it2 = edit2.iterator();
		while (it2.hasNext()) {
			String currentWord = it2.next();
			if (dict.find(currentWord) != null && dict.find(currentWord).getValue() > maxValue) {
				maxValue = dict.find(currentWord).getValue();
				suggestion = currentWord;
			}
		}
		
		if (suggestion.equals("")) {
			throw new NoSimilarWordFoundException();
		}
		
		return suggestion;
	}

}
