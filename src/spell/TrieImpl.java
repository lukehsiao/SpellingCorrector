package spell;

import java.util.*;

public class TrieImpl implements Trie {
	private int wordCount;
	private int nodeCount;
	private TrieNode[] nodes;
	private TreeMap<String, Integer> contains;

	public TrieImpl() {
		wordCount = 0;
		nodeCount = 1; //itself
		nodes = new TrieNode[26];
		contains = new TreeMap<String, Integer>();
	}
	
	public TreeSet<String> editDistance1(String w) {
		TreeSet<String> edit1 = new TreeSet<String>();
		StringBuilder word = new StringBuilder();
		
		//deletion distance
		for (int i = 0; i<w.length();i++) {
			word = new StringBuilder(w);		//start w/ original word each time
			String toAdd = word.deleteCharAt(i).toString();
			edit1.add(toAdd);
		}
		
		//transposition distance
		for (int i = 0; i<w.length()-1;i++) {
			word = new StringBuilder(w);		//start w/ original word each time
			char right = word.charAt(i);
			word.deleteCharAt(i);
			word.insert(i+1, right);
			edit1.add(word.toString());
		}
		
		//alteration distance
		for (int i = 0; i<w.length();i++) {
			for (int j = 0; j < 26; j++) {
				word = new StringBuilder(w);		//start w/ original word each time
				char insert = (char) (j+97);		//cycle through all alphabet
				word.deleteCharAt(i);
				word.insert(i, insert);
				String toAdd = word.toString();
				edit1.add(toAdd);
			}
		}
		
		
		//insertion distance
		for (int i = 0; i<=w.length();i++) {
			for (int j = 0; j < 26; j++) {
				word = new StringBuilder(w);		//start w/ original word each time
				char insert = (char) (j+97);		//cycle through all alphabet
				word.insert(i, insert);
				String toAdd = word.toString();
				edit1.add(toAdd);
			}
		}
		
		
		return edit1;
	}

	@Override
	/**
	 * Adds the specified word to the trie (if necessary) and increments the word's frequency count
	 * 
	 * @param word The word being added to the trie
	 */
	public void add(String word) {
		
		String addWord = word.toLowerCase();
		char currentChar = addWord.charAt(0);
		
		// add the word
		if (this.nodes[currentChar-97]== null) {
			this.nodes[currentChar-97] = new TrieNode();
			nodeCount++;
		}
		TrieNode lastNode = this.nodes[currentChar-97];
		for (int i = 1; i<addWord.length(); i++) {
			currentChar = addWord.charAt(i);
			if (lastNode.subNodes[currentChar-97] == null) {
				lastNode.subNodes[currentChar-97] = new TrieNode();
				nodeCount++;
			}
			lastNode = lastNode.subNodes[currentChar-97];
		}
		
		if (lastNode.getValue() == 0) {	//new word
			wordCount++;
		}
		lastNode.value++;
		
		contains.put(addWord, lastNode.value);
		
		
	}

	/**
	 * Searches the trie for the specified word
	 * 
	 * @param word The word being searched for
	 * 
	 * @return A reference to the trie node that represents the word,
	 * 			or null if the word is not in the trie
	 */
	@Override
	public Node find(String word) {
		String addWord = word.toLowerCase();
		char currentChar = addWord.charAt(0);
		
		// add the word
		if (this.nodes[currentChar-97]== null) {
			return null;
		}
		TrieNode lastNode = this.nodes[currentChar-97];
		for (int i = 1; i<addWord.length(); i++) {
			currentChar = addWord.charAt(i);
			if (lastNode.subNodes[currentChar-97] == null) {
				return null;
			}
			lastNode = lastNode.subNodes[currentChar-97];
		}
		
		if (lastNode.getValue() == 0) {	//new word
			return null;
		} else {
			return lastNode;
		}
	}

	/**
	 * Returns the number of unique words in the trie
	 * 
	 * @return The number of unique words in the trie
	 */
	@Override
	public int getWordCount() {
		// TODO Auto-generated method stub
		return wordCount;
	}

	/**
	 * Returns the number of nodes in the trie
	 * 
	 * @return The number of nodes in the trie
	 */
	@Override
	public int getNodeCount() {
		// TODO Auto-generated method stub
		return nodeCount;
	}
	/**
	 * The toString specification is as follows:
	 * For each word, in alphabetical order:
	 * <word> <count>\n
	 */
	
	@Override
	public String toString(){
		Set<String> keySet = contains.keySet();
		Iterator<String> it = keySet.iterator();
		StringBuilder output = new StringBuilder("");
		while(it.hasNext()) {
			String key = it.next();
			output.append(key + " " + contains.get(key) + "\n");
		}
		return output.toString();

	}
	
	@Override
	public int hashCode(){
		return nodeCount * wordCount + 31 * 17 / 3;
		
	}
	
	@Override
	public boolean equals(Object o){
		if (o == null) {
			return false;
		}
		if (this == o) {
			return true;
		}
		
		if (this.getClass() != o.getClass()) {
			return false;
		}
		
		TrieImpl temp = (TrieImpl) o;
		
		if (temp.hashCode() == this.hashCode()) {
			return true;
		} else {
			return false;
		}
		
	}

	/**
	 * Your trie node class should implement the Trie.Node interface
	 */
	public class TrieNode implements Node {
		private int value;
		TrieNode[] subNodes;
		public TrieNode() {
			value = 0;
			subNodes= new TrieNode[26];
		}
		
		public void setValue(int i) {
			value = i;
		}
		
		/**
		 * Returns the frequency count for the word represented by the node
		 * 
		 * @return The frequency count for the word represented by the node
		 */
		public int getValue(){
			return value;
		}
	}

}
