SpellingCorrector
=================
A simple spelling corrector implemented with a Trie.

##Introduction##

This spelling corrector not only checks against a dictionary, but, if it doesn’t find the keyword in the dictionary, it
suggests a most likely replacement. To do this it associates with every word in the dictionary a
frequency, the percent of the time that word is expected to appear in a large document. When
a word is misspelled (i.e. it is not found in the dictionary) it suggests a “similar” word
(“similar” will be defined later) whose frequency is larger or equal to any other “similar” word. 

The limitation of this spelling corrector is that it only checks a single word, rather than a list of words.

###Similarity###
A word in the dictionary is most “similar” to the input string if:  
 1. it has the “closest” edit distance from the input string.
 2. is found the most times in the dictionary.
 3. if two words are the same edit distance and have the same count/frequency, your program should prefer the one that is first alphabetically.

##Edit Distances##
There are 4 measures of edit distance this program uses: deletion distance, transposition distance,
alteration distance, and insertion distance. A word in the dictionary has an edit distance of 1
from the input string has if it has a deletion distance of 1, or a transposition distance of 1, or an
alteration distance of one, or an insertion distance or 1.

###The Four Edit Distances:###
  * **Deletion Distance 1**: A string *s* has a deletion distance 1 from another string *t* if and only if *t* is equal to *s* with one character removed. THe only strings that are a deletion distance of 1 from "bird" are "ird", "brd", "bid", and "bir". 
  * **Transposition Distance 1**: A string *s* has a transposition distance 1 from another string *t* if and only if *t* is equal to *s* with two adjacent characters transposed. The only strings that are a transposition distance of 1 from "house" are "ohuse", "huose", "hosue", and "houes".
  * **Alteration Distance 1**: A string *s* has an alteration distance 1 from another string *t* if and only if *t* is equal to *s* with exactly one character in *s* replaced by a lowercase letter that is not equal to the original letter. The only strings that are an alternation distance of 1 from "top" are "aop", "bop", ..., "zop", "tap", "tbp", ... "tzp", "toa", "tob", ..., and "toz".
  * **Insertion Distance 1**: A string *s* has an insertion distance 1 from another string *t* if and only if *t* has a deletion distance of 1 from *s*. The only strings that are an insertion distance of 1 from "ask" are "aask", "bask", "cask", ..., "zask", "aask", "absk", "acsk", ..., "azsk", "asak", "asbk", "asck", ... "aszk", "aska", "askb", ..., "askz".


##How to run the program##
Build it in Eclipse if you'd like. It is meant to run from the commandline like this:
```
java Spell dictionary.txt bbig
big

java Spell dictionary.txt hello
hello
```

